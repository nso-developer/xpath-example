WORLDFILE='world.xml'
PERFFILE='data.xml'
MEMFILE='continents.xml'

all:
	@echo "Use make init to setup the system and make clean to stop and cleanup."

build:
	make -C packages/xpath/src all

setup: build
	ncs-setup --dest .

init: setup start
	ncs_load -l -m ${WORLDFILE}

perf-data:
	./generate.py > ${PERFFILE}
	ncs_load -l -m ${PERFFILE}

memtest-data:
	./generate.py -c > ${MEMFILE}
	ncs_load -l -m ${MEMFILE}

start:
	ncs

stop:
	-ncs --stop

clean: stop
	-make -C packages/xpath/src clean
	rm -rf ${PERFFILE} ${MEMFILE} README.ncs logs ncs-cdb ncs.conf scripts state storedstate target
