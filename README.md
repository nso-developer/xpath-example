# XPath Demo

This is a short XPath demo environment that was created for the *Introduction to XPath presentation* for NSO Developer Days 2021. there is a [recording](https://www.youtube.com/watch?v=aB308LPuYOM) to go with these examples.

It contains a small [package](packages/xpath) with a simple [YANG model](packages/xpath/src/yang/xpath.yang) that can be used as a testing ground for XPath queries.

Sample data is available in [world.xml](world.xml).

There is also an example script [xpath.py](xpath.py) that you can use to explore xpath expressions. Running `./xpath` runs through a list of various xpath expressions and shows their result in your current datamodel. You can use this to experiment with various XPath expressions. Run `./xpath --help` for more options.

## YANG model

```yang
module: xpath
  +--rw world
     +--rw flags?         bits
     +--rw nice-people* [number]
     |  +--rw number    uint16
     |  +--rw name?     -> ../../people/name
     +--rw people* [name]
        +--rw name    string
        +--rw age     uint16
        +--rw hair?   enumeration
```

## Performance test

There is a simple performance test in [perf.py](perf.py). You can use [generate.py](generate.py) to generate test data (9916 elements in a list), using `./generate.py > data.xml` and then load that data into NSO (or use the `make perf-data` target).

Then run `./perf.py` to generate a small table with output for your system.

It should be reasonably straight-forward to extend perf.py with more test-cases.

## Setup

This has been tested on both NSO 5.5.1 and NSO 6.1 but should work on any recent NSO version.

Run `make init` to start the system and `make clean` stop and clean up.
