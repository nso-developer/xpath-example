#!/usr/bin/env python3
import random
import argparse

parser = argparse.ArgumentParser(
            prog='XPath test data generator')

parser.add_argument('-c', '--continents', action='store_true', help='generate continents')
args = parser.parse_args()

random.seed("XPathRules")

adjectives = [
    'pretty',
    'nice',
    'cool',
    'lame',
    'striped',
    'neon',
    'slow',
    'fast',
    'vily',
    'fluffy',
    'hard',
    'soft',
    'tall',
    'short',
    'crooked',
    'pleasant',
    'hungry',
    'evil',
    'hairy',
    'bald',
    'stubborn',
    'lonely',
    'sleepy',
    'angry',
    'alert',
    'shiny',
    'lost',
    'honest',
    'crooked',
    'perfect'
]

color = [
    'grey',
    'gray',
    'green',
    'red',
    'blue',
    'magenta',
    'copper',
    'gold',
    'silver',
    'cyan',
    'orange',
    'yellow',
    'black',
    'white',
    'lilac',
    'brown'
]

animal = [
    'cat',
    'dog',
    'zebra',
    'horse',
    'cow',
    'pig',
    'sheep',
    'llama',
    'turtle',
    'elephant',
    'caterpillar',
    'mouse',
    'rat',
    'hamster',
    'snake',
    'capybara',
    'alien',
    'ape',
    'panda',
    'guinea pig',
    'donkey'
]

continentPrefix = [
    'North',
    'South',
    'East',
    'West',
    'Central',
]
conintentSuffix = [
    'America',
    'Europe',
    'Asia',
    'Africa',
    'Australia',
    'Antarctica',
    'Atlantis',
    'Lemuria',
    'Mu',
    'Pangaea',
    'Gondwana',
    'Laurasia',
    'Rodinia',
]

preamble = """
<config xmlns="http://tail-f.com/ns/config/1.0">
  <world xmlns="http://example.com/xpath">
"""
postamble = """
  </world>
</config>
"""

def generatePeople(trim=False):
   if trim:
     adEnd = len(adjectives) // 2
     cEnd = len(color) // 2
     anEnd = len(animal) // 2
   else:
     adEnd = len(adjectives)
     cEnd = len(color)
     anEnd = len(animal)
   for adj in adjectives[:adEnd]:
    for col in color[:cEnd]:
        for ani in animal[:anEnd]:
            name = f'{adj} {col} {ani}'
            hair = random.choice(['long', 'short'])
            age  = random.randint(10, 50)
            if random.randint(0,1) == 1:
              name = name[0].upper() + name[1:]
            c = f"""
    <people>
      <name>{name}</name>
      <age>{age}</age>
      <hair>{hair}</hair>
    </people>"""
            print(c)

numElems = len(adjectives) * len(color) * len(animal)
if args.continents:
  numContinents = len(continentPrefix) * len(conintentSuffix)
  totalElems =  numElems*numContinents
#  print(f'Total number of elements {totalElems}, {numElems} each on {numContinents} continents')
print(preamble)
if args.continents:
  for cp in continentPrefix:
    for cs in conintentSuffix:
      print(f"""
    <continents>
      <name>{cp}{cs}</name>
      """)

      generatePeople(True)

      print("""
      </continents>
        """)
else:
  generatePeople()
print(postamble)
