#!/usr/bin/env python3
import ncs
import argparse

# Enabling trace is particularly useful to analyze what happens for queries that return node-sets.
parser = argparse.ArgumentParser(
            prog='XPath examples')
parser.add_argument('-t', '--trace', action='store_true', help='enable trace')
group = parser.add_mutually_exclusive_group()
group.add_argument('-a', '--all', action='store_true', help='run all examples')
group.add_argument('-s', '--section', action='append', help='run all examples in a section')
group.add_argument('-l', '--list', action='store_true', help='list all sections')

args = parser.parse_args()
traceFlag = args.trace


def trace(s):
    if traceFlag:
        print(f'TRACE: {s}')

def runx(t,xn):
    """Run an XPath xn using transaction t.

    xn can be either a simple string containing an XPath expression or a tuple in which case the first element is the 
    XPath expression and the second is a keypath pointing to the context node.
    """
    try:
        ctxt = '/'
        prfx = ''
        if len(xn) == 2:
            x = xn[0]
            ctxt = xn[1]
            prfx = f'{ctxt}: '
        else:
            x = xn
        r = t.xpath_eval_expr(x, trace, ctxt)
        print(f'{prfx}{x} -> {r}')
    except Exception as e:
        print(f'Failed with xpath {x}: {e}')

# A list of all the examples to run
examples = {
    ('bool','Boolean functions and operators'): [
        'true()',
        'false()',
        'not(true())',
        'boolean(1)',
        'boolean(true() and false())',
        'boolean(true() or false())',
        'nodeset-as-boolean(/world/people)',
        'nodeset-as-boolean(/world/people[age>100])'
    ],
    ('num','Numerical functions and operators'): [
        'floor(2.9)',
        'floor(2)',
        'ceiling(2)',
        'ceiling(2.1)',
        'round(2)',
        'round(2.1)',
        'round(2.9)',
        'sum(/world/people/age)',
        'sum(/world/people[age > 15]/age)',
        'min(/world/people/age)',
        'max(/world/people/age)',
        'avg(/world/people/age)',
        "number('123')",
        "number('one')",
        "enum-value(/world/people[name='Viktor']/hair)",
        "enum-value(/world/people[name='Chuck']/hair)",
        "compare(1,2)",
        "compare(1,-1)",
        "compare(2,1+1)",
        'band(1,3)',
        'band(1,2)',
        'bor(1,3)',
        'bor(1,2)',
        'bxor(1,3)',
        'bxor(1,2)',
        'bnot(1)',
        'bnot(2)',
        'band(1,bnot(2))',
        "bit-is-set(/world/flags, 'UP')",
        "bit-is-set(/world/flags, 'DISABLED')",
        '1+1',
        '2*2',
        '9 div 3',
        '9 div 4',
        '5 mod 3',
        '2*(2+(9 div 3))'
    ],
    ('rel', 'Relational operators'): [
        "boolean(1=1)",
        "boolean(1=2)",
        "boolean(1!=2)",
        "boolean(1!=1)",
        "boolean(1<2)",
        "boolean(1<1)",
        "boolean(1>2)",
        "boolean(2>1)",
        "boolean(1<=2)",
        "boolean(1<=1)",
        "boolean(1>=2)",
        "boolean(1>=1)"
    ],
    ('string','String functions'): [
        "starts-with('Cisco', 'C')",
        "starts-with('Cisco', 'c')",
        "contains('Cisco', 'c')",
        "contains('Cisco', 'C')",
        "string-length('Cisco')",
        "substring-before('Cisco','s')",
        "substring-after('Cisco','s')",
        "substring('Cisco',2)",
        "substring('Cisco',2, 2)",
        "substring('Cisco',2,2)",
        "string('Cisco')",
        "string(123)",
        "normalize-space(' Cisco Rocks ')",
        "normalize-space('A     B  C      D')",
        "translate('Cisco', 'C', 'D')",
        "translate('Foo Bar', 'o', 'u')",
        "translate('Foo Bar', 'oa', 'ui')",
        "translate('Foo Bar Foo', 'o', '')",
        "re-match('Cisco', '[CD]isco+')",
        "re-match('Disco', '[CD]isco+')",
        "re-match('Discot', '[CD]isco+')",
        "re-match('Cisdo', '.*isco+')",
        "string-compare('Cisco', 'Disco')",
        "string-compare('Cisco', 'Bisco')",
        "string-compare('Cisco', 'Cisco')",
        "string-compare('Cisco', 'cisco')",
    ],
    ('xml','XML-centric functions') : [
        ('name(.)', '/world'),
        ('local-name(.)', '/world'),
        ('namespace-uri(.)', '/world'),
    ],
    ('list','List functions'): [
        'count(/world/people)',
        "count(/world/people[starts-with(name, 'C')])",
        "sort-by(/world/people, 'age')",
        "deref(/world/nice-people[number=1]/name)/../age",
        "deref(/world/nice-people[number=2]/name)/../hair"
    ],
    ('pred','Predicates'): [
        '/world/people[last()]/name',
        'count(/world/people[position() > 2])',
        'count(/world/people[position() < 2])',
        ("name(current())", "/world/people{Maria}"),
        ("/world/people[age < current()/age]/name", "/world/people{Maria}")
    ],
    ('misc','Tips and Tricks'): [
        "substring-before('192.168.1.1/24', '/')",
        "substring-after('192.168.1.1/24', '/')",
        "contains('2001:db8:85a3:8d3:1319:8a2e:370:7344', ':')",
        "contains('192.168.1.1', ':')",
        "concat(substring-before('192.168.1.1','.'), '.', substring-before(substring-after('192.168.1.1','.'),'.'), '.', substring-before(substring-after(substring-after('192.168.1.1','.'),'.'),'.'), '.0')"
#        "(contains(address, '.') and cidrmask <= 32) or (contains(address, ':') and cidrmask <= 128)"
    ],
}

def runSection(key):
    print(f'\nSection: {key[1]} ({key[0]})')

    print('------------' + '-' *(len(key[1])+len(key[0])))
    for x in examples[key]:
        runx(t, x)


with ncs.maapi.single_read_trans('admin', 'test_context') as t:
    if args.list:
        print('Sections:')
        for e in examples:
            print(f'  {e[0]}: {e[1]}')
        exit(0)
    if args.section:
        for s in args.section:
            for e in examples:
                if e[0] == s:
                    runSection(e)
        exit(0)
    for e in examples:

        runSection(e)
    print('')