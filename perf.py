#!/usr/bin/env python3
import ncs
import _ncs
import time
import argparse

parser = argparse.ArgumentParser(
            prog='XPath performance tests')

parser.add_argument('-t', '--trace', action='store_true', help='enable trace')
parser.add_argument('-d', '--debug', action='store_true', help='enable debug')
args = parser.parse_args()
debugFlag = args.debug
traceFlag = args.trace

def debug(s):
    if debugFlag:
        print(f'DEBUG: {s}')

if traceFlag:
    def trace(s):
        if traceFlag:
            print(f'TRACE: {s}')
else:
    trace = None

def measure(f):
    """Measures the time the the 0-ary function f uses."""
    sumt = 0 
    t = time.perf_counter()
    f()
    dt =  time.perf_counter() - t
    return dt

def close(f, *a, **t):
    """Create a 0-ary closure of the function f with the arguments."""
    def fi():
        f(*a, **t)
    return fi


def acc(a, p, _v):
    """XPath handling function, just accumulate p into the accumulator a."""
#    a.append(str(p))
    a.add   (str(p))
    return _ncs.ITER_CONTINUE

###
# These are all pairwise examples doing the same thing in xpath and in python.
#
def count_long_hair_xpath(trans):
    cnt = trans.xpath_eval_expr("count(/world/people[hair='long'])", trace, '/')
    if debugFlag:
        print(f'DEBUG: count_long_hair_xpath: {cnt}')

def count_long_hair_xpath2(trans):
    a = set()
    def f(p, v):
        acc(a,p,v)
    res = trans.xpath_eval("/world/people[hair='long']", f, trace, '/')
    cnt = len(a)
    if debugFlag:
        print(f'DEBUG: count_long_hair_xpath2: {cnt}')

def count_long_hair_api(people):
    cnt = 0
    for p in people:
        if p.hair == 'long':
            cnt = cnt + 1
    if debugFlag:
        print(f'DEBUG: count_long_hair_api: {cnt}')

def sum_age_xpath(trans):
    res = trans.xpath_eval_expr("sum(/world/people/age)", trace, '/')
    if debugFlag:
        print(f'DEBUG: sum_age_xpath: {res}')

def sum_age_api(people):
    res = 0
    for p in people:
        res = res + p.age
    if debugFlag:
        print(f'DEBUG: sum_age_api: {res}')

def sum_age_xpath_complex(trans):
    res = trans.xpath_eval_expr("sum(/world/people[hair='long'][contains(name, 'l')]/age)", trace, '/')
    if debugFlag:
        print(f'DEBUG: sum_age_xpath_complex: {res}')

def sum_age_api_complex(people):
    res = 0
    for p in people:
        if p.hair == 'long' and 'l' in p.name:
            res = res + p.age
    if debugFlag:
        print(f'DEBUG: sum_age_api_complex: {res}')

def find_key_xpath(trans):
    res  = trans.xpath_eval_expr("/world/people[name='Pretty green llama']", trace, '/')
    if debugFlag:
        print(f'DEBUG: find_key_xpath: {res}')

def find_key_api(people):
    res = people['Pretty green llama']
    if debugFlag:
        print(f'DEBUG: find_key_api: {res}')

def filter_long_hair_xpath(trans):
    a = set()
    def f(p, v):
        acc(a,p,v)
    res = trans.xpath_eval("/world/people[hair='long']", f, trace, '/')
    if debugFlag:
        print(f'DEBUG: filter_long_hair_xpath: {len(a)}')

def filter_long_hair_api(trans):
    a = []
    for p in people:
        if p.hair == 'long':
            a.append(p._path)
    
    if debugFlag:
        print(f'DEBUG: filter_long_hair_api: {len(a)}')


def filter_name_xpath(trans):
    a = set()
    def f(p, v):
        acc(a,p,v)
    res = trans.xpath_eval("/world/people[starts-with(name, 'Pretty')]", f, trace, '/')
    if debugFlag:
        print(f'DEBUG: filter_name_xpath: {len(a)}')

def filter_name_api(people):
    a = []
    for p in people:
        if p.name.startswith('Pretty'):
            a.append(p._path)
    
    if debugFlag:
        print(f'DEBUG: filter_name_api: {len(a)}')

def filter_complex_xpath(trans):
    a = set()
    def f(p, v):
        acc(a,p,v)
    res = trans.xpath_eval("/world/people[starts-with(name, 'pretty') and (age > 25) and (hair='short')]", f, trace, '/')
    if debugFlag:
        print(f'DEBUG: filter_complex_xpath: {len(a)}')

def filter_complex_api(trans):
    a = []
    for p in people:
        if p.name.startswith('pretty') and p.age > 25 and p.hair == 'short':
            a.append(p._path)
    
    if debugFlag:
        print(f'DEBUG: filter_complex_api: {len(a)}')

def find_case_api(trans):
    value = 'Pretty grey horse'
    a = []
    for p in people:
        if p.name.upper().startswith(value.upper()):
            a.append(p._path)
    if debugFlag:
        print(f'DEBUG: find_case_api {a}')

def find_case_xpath(trans):
    value = 'Pretty GREY horse'.upper()
    a = set()
    def f(p,v):
        acc(a,p,v)
    xp = f"/world/people[translate(name, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') = '{value}']"
    res = trans.xpath_eval(xp,
            f, trace, '/')
    if debugFlag:
        print(f'DEBUG: find_case_xpath: {a}')


###
# A list of testcases. Each element is a triple of name, xpath-tester and api-tester.
#
testcases = [
    ('Sum in list, simple',                   sum_age_xpath,          sum_age_api         ),
    ('Sum in list, complex',                  sum_age_xpath_complex,  sum_age_api_complex ),
   ('Filter and accumulate in list, complex', filter_complex_xpath,   filter_complex_api  ),
   ('Filter and accumulate in list, key',     filter_name_xpath,      filter_name_api     ),
   ('Filter and accumulate in list, non-key', filter_long_hair_xpath, filter_long_hair_api),
   ('Filter and count, non-key',              count_long_hair_xpath,  count_long_hair_api ),
   ('Filter and count, non-key (2)',          count_long_hair_xpath2, count_long_hair_api ),
   ('Find key',                               find_key_xpath,         find_key_api        ),
   ('Find case-insensitive',                  find_case_xpath,        find_case_api       )
]

def run_test(test, trans, people):
    """Run a single testcase test using a transactions trans and a maagic object people pointing
       to the people-subtree in the database."""
    testname = test[0]
    fx = close(test[1], trans)
    fa = close(test[2], people)
    tx = measure(fx)
    ta = measure(fa)
    print(f'| {testname:<40} | {tx:>7.3f} | {ta:>7.3f} |')

with ncs.maapi.single_read_trans('admin', 'test_context') as trans:
    res =  trans.xpath_eval_expr("count(/world/people)", trace, '/')
    print(f"Total number of records: {res}.")
    print('+------------------------------------------+---------+---------+')
    print('|                  TEST CASE               |  XPATH  |   API   |')
    print('+------------------------------------------+---------+---------+')
    root = ncs.maagic.get_root(trans)
    people = root.world.people
    for tc in testcases:
        run_test(tc, trans, people)
    print('+------------------------------------------+---------+---------+')
